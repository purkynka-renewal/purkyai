const LANG = "cs";

const { dockStart } = require('@nlpjs/basic');

module.exports = class PurkyAI{
  constructor(context={}){
    this.updateContext(context);
  }

  updateContext(context={}){
    this.context = Object.assign({
      prijimacky: "14. a 16. dubna",
      obory: "#neni-definovano"
    }, context);
  }

  async load(){
    const dock  = await dockStart({ use: ['Basic']});
    this.nlp    = dock.get('nlp');
    await this.nlp.load(__dirname+"/model.nlp");
  }

  process(input=""){
    return this.nlp.process(LANG, String(input), this.context);
  }
}
