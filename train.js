const fs    = require('fs');
const YAML  = require('yaml');
const { dockStart } = require('@nlpjs/basic');


const LANG = "cs";


async function train(log=true) {
  const dock = await dockStart({ use: ['Basic', 'LangCs'], settings:{
    nlp: {
      autoLoad: false, autoSave: false,
    },
    'nlu-manager':{
      spellCheck: true,
      spellCheckDistance: 10,
      log: log
    }
  }});
  const nlp  = dock.get('nlp');
  const stemmer = dock.get('stemmer-cs');
  stemmer.limit = 4; // Fix stemming issue
  nlp.addLanguage(LANG);

  const inputs  = YAML.parse(
    fs.readFileSync(__dirname+"/traindata/inputs.yml" ).toString()
  );
  const outputs = YAML.parse(
    fs.readFileSync(__dirname+"/traindata/outputs.yml").toString()
  );

  // load inputs
  for(const catName in inputs){
    const cat = inputs[catName];
    for(const key in cat){
      const values = Array.isArray(cat[key]) ? cat[key] : [cat[key]];
      for(let value of values){
        // // Fix stemming issue in lang-cs
        // value = value.replace(/([aáeéěiíoóuůúyý])([ ?!.:;\-]|$)/g, (_,a,b)=>(
        //   a+'a'+b
        // ));

        nlp.addDocument(LANG, value, catName+'.'+key);
      }
    }
  }

  // load outputs
  for(const catName in outputs){
    const cat = outputs[catName];
    if(typeof cat !== 'object' || Array.isArray(cat)){
      _add(cat, catName);
      continue;
    }
    for(const key in cat){
      _add(cat[key], catName+'.'+key);
    }

    function _add(value, name) {
      const values = Array.isArray(value) ? value : [value];
      for(const value of values){
        nlp.addAnswer(LANG, name, value);
      }
    }
  }

  await nlp.train();
  await nlp.save(__dirname+"/model.nlp", true);
}

if(require.main === module){
  train();
}else{
  module.exports = train;
}
