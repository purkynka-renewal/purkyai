const PurkyAI = require("./");

const purkyAI = new PurkyAI();
purkyAI.load().then(()=>{
  console.log("Ready");
  process.stdout.write("> ");
  process.stdin.on("data", async (data)=>{
    const result = await purkyAI.process(data);
    console.log(result.intent+': '+result.answer);
    process.stdout.write("> ");
  });
});
